---
breadcrumbs:
- - /Home
  - Chromium
- - /Home/chromium-security
  - Chromium Security
page_name: hall-of-fame
title: Security Hall of Fame
---

## The following bugs qualified for a Chromium Security Reward, or represent a win at our Pwnium competition. On behalf of our millions of users, we thank the named researchers for helping make Chromium safer.

**This information is historical and isn't being updated**.

*   $60000 to Sergey Glazunov for [bug
            117226](http://code.google.com/p/chromium/issues/detail)
*   $60000 to PinkiePie for [bug
            117620](http://code.google.com/p/chromium/issues/detail)
*   $40000 to PinkiePie for [bug
            181083](http://code.google.com/p/chromium/issues/detail) and others
*   $31336 to Ralf-Philipp Weinmann for [bug 227181](http://code.google.com/p/chromium/issues/detail) and others
*   $30000 to someone who wishes to remain anonymous
*   $30000 to someone who wishes to remain anonymous
*   $21500 to Andrey Labunets for [bug 252062](http://code.google.com/p/chromium/issues/detail) and others
*   $10000 to miaubiz for [bug
            116661](http://code.google.com/p/chromium/issues/detail)
*   $10000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            116662](http://code.google.com/p/chromium/issues/detail)
*   $10000 to Arthur Gerkis for [bug
            116663](http://code.google.com/p/chromium/issues/detail)
*   $10000 to Sergey Glazunov for [bug
            143439](http://code.google.com/p/chromium/issues/detail)
*   $10000 to miaubiz for [bug
            157047](http://code.google.com/p/chromium/issues/detail)
*   $10000 to Atte Kettunen for [bug
            157048](http://code.google.com/p/chromium/issues/detail)
*   $10000 to Christian Holler for [bug
            157049](http://code.google.com/p/chromium/issues/detail)
*   $7331 to PinkiePie for [bug
            162835](http://code.google.com/p/chromium/issues/detail)
*   $5000 to João Lucas Melo Brasio from [White Hat Hackers Consultoria
            de Segurança da Informação LTDA](http://www.whitehathackers.com.br/)
            for [bug 321940](http://code.google.com/p/chromium/issues/detail)
            and others
*   $4000 + $500 to Sergey Glazunov for [bug
            143437](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Atte Kettunen for bug 179522
*   $3133.7 to Atte Kettunen for bug 147499
*   $3133.7 to Collin Payne for [bug
            242762](https://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Collin Payne for [bug
            244746](https://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Sergey Glazunov for [bug
            68666](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Sergey Glazunov for [bug
            83275](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to miaubiz for [bug
            88944](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Chamal de Silva for [bug
            107182](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            108071](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Arthur Gerkis for [bug
            128178](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to Arthur Gerkis for [bug
            135043](http://code.google.com/p/chromium/issues/detail)
*   $3133.7 to miaubiz for [bug
            141901](http://code.google.com/p/chromium/issues/detail)
*   $2000 + $500 to Sergey Glazunov for [bug
            98053](http://code.google.com/p/chromium/issues/detail)
*   $2000 + $500 to Sergey Glazunov for [bug
            99512](http://code.google.com/p/chromium/issues/detail)
*   $2000 + $500 to Sergey Glazunov for [bug
            99750](http://code.google.com/p/chromium/issues/detail)
*   $2337 to Sergey Glazunov for [bug
            93906](http://code.google.com/p/chromium/issues/detail)
*   $2337 to Sergey Glazunov for [bug
            96047](http://code.google.com/p/chromium/issues/detail)
*   $2337 to Sergey Glazunov for [bug
            96885](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Atte Kettunen for bug 279277
*   $2000 to Atte Kettunen for bug 265838
*   $2000 to Sergey Glazunov for [bug
            93416](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Sergey Glazunov for [bug
            95671](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Daniel Divricean for [bug
            93416](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Sergey Glazunov for [bug
            117550](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Chamal de Silva for [bug
            139814](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Christian Schneider for [bug
            380885](https://bugs.chromium.org/p/chromium/issues/detail)
            ([writeup](https://christian-schneider.net/ChromeSopBypassWithSvg.html#main))
*   $1500 to Atte Kettunen for bug 150729
*   $1337 to Sergey Glazunov for [bug
            35724](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Sergey Glazunov for [bug
            45400](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Sergey Glazunov for [bug
            50553](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Keith Campbell for [bug
            51630](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            59036](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Sergey Glazunov for [bug
            65764](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Sergey Glazunov for [bug
            70165](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Daniel Divricean for [bug
            69187](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Daniel Divricean for [bug
            70877](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Vincenzo Iozzo, Ralf Philipp Weinmann and Willem Pinckaers,
            through ZDI, for [bug
            75712](http://code.google.com/p/chromium/issues/detail)
*   $1337 to kuzzcc for [bug
            77026](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Michael Braithwaite of Turbulenz Limited for [bug
            89836](http://code.google.com/p/chromium/issues/detail)
*   $1337 to miaubiz for [bug
            100059](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $1000 to Sergey Glazunov for [bug
            73196](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $1000 to Sergey Glazunov for [bug
            73595](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $1000 to Sergey Glazunov for [bug
            74991](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $1000 to Sergey Glazunov for [bug
            77463](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            73746](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            74562](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            75170](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            79199](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            89520](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            90222](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            91598](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            97451](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            97520](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            97615](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            97784](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Sergey Glazunov for [bug
            98407](http://code.google.com/p/chromium/issues/detail)
*   $1000 + $500 to Chamal de Silva for [bug
            171951](https://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen for bug 235733
*   $1000 to Atte Kettunen for bug 292422
*   $1000 to Atte Kettunen for bug 271939
*   $1000 to Atte Kettunen for bug 223238
*   $1000 to Atte Kettunen for bug 172926
*   $1000 to Atte Kettunen for bug 172342
*   $1000 to Atte Kettunen for bug 172331
*   $1000 to Atte Kettunen for bug 172243
*   $1000 to Atte Kettunen for bug 168768
*   $1000 to Atte Kettunen for bug 162551
*   $1000 to Atte Kettunen for bug 162494
*   $1000 to Atte Kettunen for bug 161077
*   $1000 to Atte Kettunen for bug 159338
*   $1000 to Atte Kettunen for bug 156231
*   $1000 to Atte Kettunen for bug 154055
*   $1000 to Atte Kettunen for bug 152707
*   $1000 to Atte Kettunen for bug 151008
*   $1000 to Atte Kettunen for bug 138208
*   $1000 to Atte Kettunen for bug 133571
*   $1000 to Atte Kettunen for bug 133214
*   $1000 to Atte Kettunen for bug 130240
*   $1000 to Tokuji Akamine for [bug
            30660](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            37383](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jordi Chancel for [bug
            40445](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            39985](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            39047](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            45983](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Mike Taylor for [bug
            49964](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            50515](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            51835](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            51654](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            51670](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            48437](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            52204](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            50386](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Ashutosh Mehra and Vineet Batra of the Adobe Reader Sandbox
            Team for [bug
            52682](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            50712](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Stefano Di Paola of MindedSecurity for [bug
            55350](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            54691](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            56760](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            61338](http://code.google.com/p/chromium/issues/detail)
*   $1000 to wushi of team509 for [bug
            55257](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            58657](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            59320](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christoph Diehl for [bug
            60055](http://code.google.com/p/chromium/issues/detail)
*   $1000 to wushi of team509 for [bug
            60688](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            62623](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            62127](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            62401](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Chris Rohlf for [bug
            63866](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            64959](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            64945](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            66560](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jared Allar of [CERT](http://www.cert.org/) for [bug
            67208](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            67303](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            67393](http://code.google.com/p/chromium/issues/detail)
*   $1000 to David Warren of [CERT](http://www.cert.org/) for [bug
            68115](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            68170](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68178](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68181](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            68439](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68558](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            63248](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            55831](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            64051](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            65577](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68641](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            68120](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            69556](http://code.google.com/p/chromium/issues/detail)
*   $1000 to David Warren of [CERT](http://www.cert.org/) for [bug
            70456](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jordi Chancel for [bug
            54262](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68263](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            68741](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            70244](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            71114](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            71115](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            71296](http://code.google.com/p/chromium/issues/detail)
*   $1000 to wushi of team509 for [bug
            71386](http://code.google.com/p/chromium/issues/detail)
*   $1000 to wushi of team509 for [bug
            71388](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            71595](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            71855](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Chamal de Silva for [bug
            72437](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            73235](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            70027](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            70442](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            71763](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            72028](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            73066](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            73134](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            74030](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74662](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74675](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            73216](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74660](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74665](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74666](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74669](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74671](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74672](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74673](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            74678](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christoph Diehl for [bug
            78524](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            73526](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            74653](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jose A. Vazquez for [bug
            75186](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            75801](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            76001](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            76666](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            77507](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            78031](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            76966](http://code.google.com/p/chromium/issues/detail)
*   $1000 to wushi of team509 for [bug
            77130](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Marek Majkowski for [bug
            77346](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            72340](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            78071](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            78270](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            76059](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            72387](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            73962](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            79746](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            81949](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Vladislavas Jarmalis for [bug
            83010](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            83743](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            84452](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            82546](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            84234](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Philippe Arteau for [bug
            77493](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            84355](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            85003](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            85211](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            85418](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Chamal de Silva for [bug
            88850](http://code.google.com/p/chromium/issues/detail)
*   $1000 to the Microsoft Java Team and Microsoft Vulnerability
            Research (MSVR) for [bug
            88093](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            78841](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            78841](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            86502](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            87148](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            87227](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            87729](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            87925](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            88591](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            88846](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Martin Barbella for [bug
            88889](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Vladimir Vorontsov, [ONsec company](http://onsec.ru) for
            [bug 72492](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            88216](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sergey Glazunov for [bug
            87453](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            90668](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            91665](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            89219](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            89330](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            92651](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            92959](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            93420](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            93587](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            95920](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            93788](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            95072](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            95672](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            98773](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            99167](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            97599](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            98064](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            98556](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            99294](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            99880](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            96902](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            99138](http://code.google.com/p/chromium/issues/detail)
*   $1000 x2 to miaubiz for [bug
            99211](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            100464](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            97092](http://code.google.com/p/chromium/issues/detail)
*   $1000 to kuzzcc for [bug
            62925](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            101458](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Christian Holler for [bug
            103259](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            103058](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            102810](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            102628](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            102359](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            103921](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            104011](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            104859](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            104056](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            103239](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            104529](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Luka Treiber of ACROS Security for [bug
            99016](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            106441](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Shawn Goertzen for [bug
            108871](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            109716](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            109743](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            110112](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            110374](http://code.google.com/p/chromium/issues/detail)
*   $1000 x2 to miaubiz for [bug
            105459](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            106484](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            108605](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            109556](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Boris Zbarsky of Mozilla for [bug
            106672](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            108695](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            110172](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            111779](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            112847](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Chamal de Silva for [bug
            105867](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            111748](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            108037](http://code.google.com/p/chromium/issues/detail)
*   $2000 to Arthur Gerkis for [bug
            112212](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            116093](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            108406](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            115471](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            113258](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            113439](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            114924](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            115028](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            113497](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            113707](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            114068](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            114219](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            115681](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            113902](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            116746](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            116461](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            104863](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            107758](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            108207](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Chamal de Silva for [bug
            108544](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            107244](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            110764](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            112151](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            112833](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            111467](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            112411](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            114342](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            113837](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            117471](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Omair for [bug
            117588](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            135432](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            140803](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            143609](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            143656](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Sławomir Błażek for [bug
            144899](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            145544](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            134897](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Arthur Gerkis for [bug
            136235](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jüri Aedla for [bug
            136894](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            129898](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            130595](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            120222](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            120944](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            124356](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            125374](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            129947](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            129951](http://code.google.com/p/chromium/issues/detail)
*   $1000 to miaubiz for [bug
            130356](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Jüri Aedla for [bug
            132779](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Collin Payne for [bug
            150737](https://code.google.com/p/chromium/issues/detail)
*   $509 to wushi of team509 for [bug
            34978](http://code.google.com/p/chromium/issues/detail)
*   $500 to Hironori Tokuta for bug 169401
*   $500 to Atte Kettunen for bug 284785
*   $500 to Atte Kettunen for bug 271161
*   $500 to Atte Kettunen for bug 270758
*   $500 to Atte Kettunen for bug 223962
*   $500 to Atte Kettunen for bug 152569
*   $500 to Atte Kettunen for bug 253550
*   $500 to Atte Kettunen for bug 167069
*   $500 to Atte Kettunen for bug 148638
*   $500 to Atte Kettunen for bug 142169
*   $500 to Isaac Dawson for [bug
            21338](http://code.google.com/p/chromium/issues/detail)
*   $500 to Billy Rios for [bug
            26129](http://code.google.com/p/chromium/issues/detail)
*   $500 to Timothy D. Morgan of [VSR](http://www.vsecurity.com/) for
            [bug 32718](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            35732](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            37061](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            39443](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            40635](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            42294](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            42723](http://code.google.com/p/chromium/issues/detail)
*   $500 to Rodrigo Marcos of SECFORCE for [bug
            46126](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            44424](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            46360](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            48115](http://code.google.com/p/chromium/issues/detail)
*   $500 to Michail Nikolaev for [bug
            42736](http://code.google.com/p/chromium/issues/detail)
*   $500 to sp3x of [SecurityReason.com](http://securityreason.com/) for
            [bug 43813](http://code.google.com/p/chromium/issues/detail)
*   $500 to [Jose A. Vazquez](http://spa-s3c.blogspot.com/) for [bug
            47866](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            48284](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            49596](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            49628](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            45331](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            51653](http://code.google.com/p/chromium/issues/detail)
*   $500 to Isaac Dawson for [bug
            53001](http://code.google.com/p/chromium/issues/detail)
*   $500 to David Weston of Microsoft Vulnerability Research (MSVR) for
            [bug 50250](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            51252](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            51919](http://code.google.com/p/chromium/issues/detail)
*   $500 to Sergey Glazunov for [bug
            53361](http://code.google.com/p/chromium/issues/detail)
*   $500 to remy.saissy for [bug
            53361](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            53394](http://code.google.com/p/chromium/issues/detail)
*   $500 to wushi of team509 for [bug
            55114](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            57501](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            51680](http://code.google.com/p/chromium/issues/detail)
*   $500 to Simon Schaak for [bug
            54500](http://code.google.com/p/chromium/issues/detail)
*   $500 to "vkouchna" for [bug
            58741](http://code.google.com/p/chromium/issues/detail)
*   $500 to "gundlach" / various for [bug
            60238](http://code.google.com/p/chromium/issues/detail)
*   $500 to "fam.lam" for [bug
            60327](http://code.google.com/p/chromium/issues/detail)
*   $500 to Stefan Troger for [bug
            59554](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            63051](http://code.google.com/p/chromium/issues/detail)
*   $500 to Sławomir Błażek for [bug
            62956](http://code.google.com/p/chromium/issues/detail)
*   $500 to Chamal de Silva for [bug
            65299](http://code.google.com/p/chromium/issues/detail)
*   $500 to Jan Tošovský for [bug
            66748](http://code.google.com/p/chromium/issues/detail)
*   $500 to *anonymous* for [bug
            67363](http://code.google.com/p/chromium/issues/detail)
*   $500 to Sergey Radchenko for [bug
            63732](http://code.google.com/p/chromium/issues/detail)
*   $500 to Stefan van Zanden for [bug
            70078](http://code.google.com/p/chromium/issues/detail)
*   $500 to Martin Barbella for [bug
            69628](http://code.google.com/p/chromium/issues/detail)
*   $500 to Daniel Divricean for [bug
            70336](http://code.google.com/p/chromium/issues/detail)
*   $500 to Alex Turpin for [bug
            72517](http://code.google.com/p/chromium/issues/detail)
*   $500 to Christian Holler for [bug
            74670](http://code.google.com/p/chromium/issues/detail)
*   $500 to Yuri Ko for [bug
            70070](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin for [bug
            71586](http://code.google.com/p/chromium/issues/detail)
*   $500 to Michael Griffiths for [bug
            75347](http://code.google.com/p/chromium/issues/detail)
*   $500 to Dan Rosenberg for [bug
            76542](http://code.google.com/p/chromium/issues/detail)
*   $500 to Jordi Chancel for [bug
            77786](http://code.google.com/p/chromium/issues/detail)
*   $500 to Collin Payne for [bug
            81916](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin from
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            85177](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            85102](http://code.google.com/p/chromium/issues/detail)
*   $500 to Mario Gomes for [bug
            85808](http://code.google.com/p/chromium/issues/detail)
*   $500 to kuzzcc for [bug
            85808](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            87298](http://code.google.com/p/chromium/issues/detail)
*   $500 to Shih Wei-Long for [bug
            87339](http://code.google.com/p/chromium/issues/detail)
*   $500 to Juho Nurminen for [bug
            88337](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 89142](http://code.google.com/p/chromium/issues/detail)
*   $500 to Mario Gomes for [bug
            78639](http://code.google.com/p/chromium/issues/detail)
*   $500 to Jordi Chancel for [bug
            89564](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            89991](http://code.google.com/p/chromium/issues/detail)
*   $500 to Christian Holler for [bug
            91120](http://code.google.com/p/chromium/issues/detail)
*   $500 to Jordi Chancel for [bug
            86758](http://code.google.com/p/chromium/issues/detail)
*   $500 to Simon Sarris for [bug
            91016](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 100465](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 98809](http://code.google.com/p/chromium/issues/detail)
*   $500 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            104959](http://code.google.com/p/chromium/issues/detail)
*   $500 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            105714](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 108416](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 108901](http://code.google.com/p/chromium/issues/detail)
*   $500 to Aki Helin of [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 110277](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            106336](http://code.google.com/p/chromium/issues/detail)
*   $500 to pa_kt for [bug
            112259](http://code.google.com/p/chromium/issues/detail)
*   $500 to Sławomir Błażek for [bug
            112670](http://code.google.com/p/chromium/issues/detail)
*   $500 to Sławomir Błażek for [bug
            114054](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            108467](http://code.google.com/p/chromium/issues/detail)
*   $500 to Masato Kinugawa for [bug
            109574](http://code.google.com/p/chromium/issues/detail)
*   $500 to Arthur Gerkis for [bug
            112317](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            114056](http://code.google.com/p/chromium/issues/detail)
*   $500 to Christian Holler for [bug
            117794](http://code.google.com/p/chromium/issues/detail)
*   $500 to Nir Moshe for [bug
            137707](http://code.google.com/p/chromium/issues/detail)
*   $500 to pawlkt for [bug
            139168](http://code.google.com/p/chromium/issues/detail)
*   $500 to Atte Kettunen of
            [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for [bug
            141651](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            121347](http://code.google.com/p/chromium/issues/detail)
*   $500 to miaubiz for [bug
            136881](http://code.google.com/p/chromium/issues/detail)
*   $500 to Emmanuel Bronshtein for [bug
            142956](http://code.google.com/p/chromium/issues/detail)
*   $500 to Takeshi Terada for [bug
            144813](http://code.google.com/p/chromium/issues/detail)
*   $500 to Takeshi Terada for [bug
            144820](http://code.google.com/p/chromium/issues/detail)
*   $500 to Takeshi Terada for [bug
            137532](http://code.google.com/p/chromium/issues/detail)
*   $500 to Takeshi Terada for [bug
            144866](http://code.google.com/p/chromium/issues/detail)
*   $500 to Takeshi Terada for [bug
            141889](http://code.google.com/p/chromium/issues/detail)
*   $500 to Artem Chaykin for [bug
            138210](http://code.google.com/p/chromium/issues/detail)
*   $500 to Artem Chaykin for [bug
            138035](http://code.google.com/p/chromium/issues/detail)
*   $500 to Ayush Jindal for [bug
            68342](https://code.google.com/p/chromium/issues/detail)

The following special-case rewards were issued for bugs in components external
to the Chromium project. We sometimes issue rewards for bugs in external
components where information of the bug enabled us to proactively protect our
users.

*   $5000 to Eetu Luodemaa and Joni Vähämäki, both from Documill, for
            [bug 146254](http://code.google.com/p/chromium/issues/detail)
*   $4000 to Jüri Aedla for [bug
            107128](http://code.google.com/p/chromium/issues/detail)
*   $3000 to Jüri Aedla for [bug
            129930](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Marc Schoenefeld for [bug
            48283](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Simon Berry-Byrne for [bug
            48733](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Marc Schoenefeld for [bug
            51070](http://code.google.com/p/chromium/issues/detail)
*   $1337 to Jüri Aedla for [bug
            112822](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Bui Quang Minh from Bkis
            ([www.bkis.com](http://www.bkis.com)) for [bug
            58731](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Yang Dingning from NCNIPC, Graduate University of Chinese
            Academy of Sciences for [bug
            63444](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Yang Dingning from NCNIPC, Graduate University of Chinese
            Academy of Sciences for [bug
            86900](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Yang Dingning from NCNIPC, Graduate University of Chinese
            Academy of Sciences for [bug
            89402](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Yang Dingning from NCNIPC, Graduate University of Chinese
            Academy of Sciences for [bug
            93472](http://code.google.com/p/chromium/issues/detail)
*   $1000 to Nicolas Gregoire for [bug
            138673](http://code.google.com/p/chromium/issues/detail)
*   $500 to Nicolas Gregoire for [bug
            127417](http://code.google.com/p/chromium/issues/detail)

(Note that some of the above individuals elected to donate the rewards to
charity. In these cases, Google often increased the value of the donation beyond
the stated reward amount.)

### **Honorable mention**

The following lower severity or duplicate bugs were responsibly reported to the
Chromium project. Thanks to these named individuals for helping Chromium
security!

*   Mike Dougherty of dotSyntax, LLC for [bug
            33572](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            37007](http://code.google.com/p/chromium/issues/detail)
*   RSnake of [ha.ckers.org](http://ha.ckers.org/) for [bug
            33445](http://code.google.com/p/chromium/issues/detail)
*   Tobias Klein ([www.trapkit.de](http://www.trapkit.de/)) for [bug
            38845](http://code.google.com/p/chromium/issues/detail)
*   Carlos Ghan for [bug
            33952](http://code.google.com/p/chromium/issues/detail)
*   WHK &lt;elhacker.net&gt; and The-0utl4w From Aria-Security for [bug
            34721](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 35979](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 36976](http://code.google.com/p/chromium/issues/detail)
*   Jordi Chancel for [bug
            37447](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            39277](http://code.google.com/p/chromium/issues/detail)
*   Florent, Skyrecon systems for [bug
            40801](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            41778](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 42538](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            40628](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            41447](http://code.google.com/p/chromium/issues/detail)
*   Florian Rienhardt, BSI for [bug
            36553](http://code.google.com/p/chromium/issues/detail)
*   Ben Davis and Emanuele Gentili for [bug
            38105](http://code.google.com/p/chromium/issues/detail)
*   Sergey Glazunov for [bug
            42396](http://code.google.com/p/chromium/issues/detail)
*   Jose A. Vazquez for [bug
            45164](http://code.google.com/p/chromium/issues/detail)
*   Mats Ahlgren for [bug
            46575](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 47056](http://code.google.com/p/chromium/issues/detail)
*   Lostmon for [bug
            28001](http://code.google.com/p/chromium/issues/detail)
*   "ironfist99" for [bug
            34414](http://code.google.com/p/chromium/issues/detail)
*   Chris Weber from Casaba Security for [bug
            37201](http://code.google.com/p/chromium/issues/detail)
*   Brook Novak for [bug
            41654](http://code.google.com/p/chromium/issues/detail)
*   Lostmon for [bug
            45876](http://code.google.com/p/chromium/issues/detail)
*   Keith Campbell for [bug
            51846](http://code.google.com/p/chromium/issues/detail)
*   [VUPEN Vulnerability Research Team](http://www.vupen.com/)
            (VUPEN-SR-2010-249) for [bug
            52443](http://code.google.com/p/chromium/issues/detail)
*   "magnusmorton" for [bug
            51709](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            53176](http://code.google.com/p/chromium/issues/detail)
*   "adriennefelt" for [bug
            54006](http://code.google.com/p/chromium/issues/detail)
*   Jordi Chancel for [bug
            51680](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            53002](http://code.google.com/p/chromium/issues/detail)
*   Dan Rosenberg, Virtual Security Research for [bug
            54132](http://code.google.com/p/chromium/issues/detail)
*   Nirankush Panchbhai and Microsoft Vulnerability Research (MSVR) for
            [bug 55745](http://code.google.com/p/chromium/issues/detail)
*   Cezary Tomczak for [bug
            58319](http://code.google.com/p/chromium/issues/detail)
*   Mohammed Bouhlel for [bug
            61701](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            62168](http://code.google.com/p/chromium/issues/detail)
*   David Weston of Microsoft Vulnerability Research (MSVR) for [bug
            60055](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            60761](http://code.google.com/p/chromium/issues/detail)
*   Marius Wachtler for [bug
            62276](http://code.google.com/p/chromium/issues/detail)
*   Chamal de Silva for [bug
            65299](http://code.google.com/p/chromium/issues/detail)
*   Chamal de Silva for [bug
            66591](http://code.google.com/p/chromium/issues/detail)
*   David Warren of [CERT](http://www.cert.org/) for [bug
            67303](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            67363](http://code.google.com/p/chromium/issues/detail)
*   Brian Kirchoff for [bug
            62791](http://code.google.com/p/chromium/issues/detail)
*   Dan Morrison for [bug
            66931](http://code.google.com/p/chromium/issues/detail)
*   Matthew Heidermann for [bug
            68244](http://code.google.com/p/chromium/issues/detail)
*   [Reddit](http://www.reddit.com) for [bug
            69195](http://code.google.com/p/chromium/issues/detail)
*   Rik Cabanier for [bug
            67234](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            71717](http://code.google.com/p/chromium/issues/detail)
*   Louis Lang for [bug
            49747](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 66962](http://code.google.com/p/chromium/issues/detail)
*   David Weston of Microsoft and Microsoft Vulnerability Research
            (MSVR) for [bug
            71788](http://code.google.com/p/chromium/issues/detail)
*   Martin Barbella for [bug
            61502](http://code.google.com/p/chromium/issues/detail)
*   Chamal de Silva for [bug
            70538](http://code.google.com/p/chromium/issues/detail)
*   Cole Snodgrass for [bug
            72523](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            72910](http://code.google.com/p/chromium/issues/detail)
*   wushi of team509 for [bug
            75801](http://code.google.com/p/chromium/issues/detail)
*   wushi of team509 for [bug
            76646](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            77349](http://code.google.com/p/chromium/issues/detail)
*   Sergey Glazunov for [bug
            75821](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            79266](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            79426](http://code.google.com/p/chromium/issues/detail)
*   Sergey Glazunov for [bug
            83273](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            83841](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            84402](http://code.google.com/p/chromium/issues/detail)
*   Olli Pettay of Mozilla for [bug
            84600](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            84805](http://code.google.com/p/chromium/issues/detail)
*   Mikołaj Małecki for [bug
            85559](http://code.google.com/p/chromium/issues/detail)
*   Collin Payne for [bug
            84933](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            82552](http://code.google.com/p/chromium/issues/detail)
*   wushi of team509 (reported through ZDI) for [bug
            88670](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            88670](http://code.google.com/p/chromium/issues/detail)
*   electronixtar for [bug
            51464](http://code.google.com/p/chromium/issues/detail)
*   wbrana for [bug
            57908](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            78427](http://code.google.com/p/chromium/issues/detail)
*   kuzzcc for [bug
            83031](http://code.google.com/p/chromium/issues/detail)
*   Aaron Sigel of [vtty.com](http://vtty.com) for [bug
            80680](http://code.google.com/p/chromium/issues/detail)
*   Mario Gomes for [bug
            85041](http://code.google.com/p/chromium/issues/detail)
*   Arthur Gerkis for [bug
            89795](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            90134](http://code.google.com/p/chromium/issues/detail)
*   miaubiz for [bug
            94800](http://code.google.com/p/chromium/issues/detail)
*   Bernhard 'Bruhns' Brehm of Recurity Labs for [bug
            93497](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 93596](http://code.google.com/p/chromium/issues/detail)
*   Nishant Yadant of VMware and Craig Chamberlain for [bug
            95917](http://code.google.com/p/chromium/issues/detail)
*   Chu for [bug
            101010](http://code.google.com/p/chromium/issues/detail)
*   Aki Helin from [OUSPG](https://www.ee.oulu.fi/research/ouspg/) for
            [bug 100863](http://code.google.com/p/chromium/issues/detail)
*   Collin Payne for [bug
            92550](http://code.google.com/p/chromium/issues/detail)
*   Devdatta Akhawe, UC Berkeley for [bug
            103630](http://code.google.com/p/chromium/issues/detail)
*   Atte Kettunen from [OUSPG](https://www.ee.oulu.fi/research/ouspg/)
            for [bug 109094](http://code.google.com/p/chromium/issues/detail)
*   Code Audit Labs of VulnHunt.com for [bug
            109245](http://code.google.com/p/chromium/issues/detail)
*   Sławomir Błażek for [bug
            109664](http://code.google.com/p/chromium/issues/detail)
*   Ben Carrillo for [bug
            109717](http://code.google.com/p/chromium/issues/detail)
*   chrometot for [bug
            112451](http://code.google.com/p/chromium/issues/detail)
*   Michael Gundlach for [bug
            108648](http://code.google.com/p/chromium/issues/detail)
*   Glenn Randers-Pehrson for [bug
            116162](http://code.google.com/p/chromium/issues/detail)