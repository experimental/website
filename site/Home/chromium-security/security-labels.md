---
breadcrumbs:
- - /Home
  - Chromium
- - /Home/chromium-security
  - Chromium Security
page_name: security-labels
title: Security labels/components
---

Please see
[security-labels.md](https://chromium.googlesource.com/chromium/src/+/master/docs/security/security-labels.md).