---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/telemetry
  - 'Telemetry: Introduction'
page_name: performance-try-bots
title: Performance Try Bots
---

#### Latest docs are at: <https://chromium.googlesource.com/chromium/src/+/master/docs/speed/perf_trybots.md>