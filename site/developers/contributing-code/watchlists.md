---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/contributing-code
  - Contributing Code
page_name: watchlists
title: Watchlists
---

This document has migrated to
<https://chromium.googlesource.com/chromium/src/+/master/docs/infra/watchlists.md>