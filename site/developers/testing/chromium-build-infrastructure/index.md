---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/testing
  - Testing and infrastructure
page_name: chromium-build-infrastructure
title: Chromium build infrastructure
---

<img alt="image" src="http://www.google.com/chart" height=300 width=500>