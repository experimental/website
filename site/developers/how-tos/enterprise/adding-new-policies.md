---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/how-tos
  - How-Tos
- - /developers/how-tos/enterprise
  - Enterprise
page_name: adding-new-policies
title: Policy Settings in Chrome
---

**This documentation has been moved to**

<https://chromium.googlesource.com/chromium/src/+/master/docs/enterprise/add_new_policy.md>