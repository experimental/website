---
breadcrumbs:
- - /developers
  - For Developers
page_name: how-tos
title: How-Tos
---

*   Build instructions:
            [Windows](https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md),
            [Mac OS
            X](https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md),
            [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_build_instructions.md),
            [ChromeOS](http://www.chromium.org/developers/how-tos/build-instructions-chromeos),
            [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_build_instructions.md),
            and
            [iOS](https://chromium.googlesource.com/chromium/src/+/master/docs/ios/build_instructions.md)
*   Debugging instructions:
            [Windows](/developers/how-tos/debugging-on-windows), [Mac OS
            X](https://chromium.googlesource.com/chromium/src/+/master/docs/mac/debugging.md),
            [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux/debugging.md),
            and
            [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_debugging_instructions.md)
*   [Running Chrome
            tests](http://code.google.com/p/chromium/wiki/RunningChromeUITests)
*   [Linux
            Development](http://code.google.com/p/chromium/wiki/LinuxDevelopment)
            tips and porting guide
*   [Using Git](http://code.google.com/p/chromium/wiki/UsingGit) for
            version control and code reviews

For more, see this list of sub-pages:

<img alt="image" src="http://www.google.com/chart" height=300 width=500>