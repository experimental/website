---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/how-tos
  - How-Tos
page_name: android-build-instructions
title: Build Instructions (Android)
---

This page has been replaced by
<https://chromium.googlesource.com/chromium/src/+/master/docs/android_build_instructions.md>