---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/how-tos
  - How-Tos
page_name: get-the-code
title: 'Get the Code: Checkout, Build, & Run Chromium'
---

## *If you work at Google, you probably want to read the [Google-specific instructions](https://goto.corp.google.com/building-chrome) instead, which are basically the same except for some details relating to the Google corporate computer images.*

## Chromium supports building on Windows, Mac and Linux host systems. Linux is required for building Android, and a Mac is required for building iOS.

Click on one of these links depending on what you want to build.

*   [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux/build_instructions.md)
*   [Windows](https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md)
*   [Mac](https://chromium.googlesource.com/chromium/src/+/master/docs/mac_build_instructions.md)
*   Chrome OS
    *   [linux-chromeos](https://chromium.googlesource.com/chromium/src/+/master/docs/chromeos_build_instructions.md)
                (runs the Chrome OS version of Chrome on Linux)
    *   [cros-vm](https://chromium.googlesource.com/chromiumos/docs/+/master/cros_vm.md)
                (runs in a Chrome OS virtual machine)
    *   ["simplechrome"](https://chromium.googlesource.com/chromiumos/docs/+/master/simple_chrome_workflow.md)
                (runs on Chromebook hardware)
*   [iOS](https://chromium.googlesource.com/chromium/src/+/master/docs/ios/build_instructions.md)
*   Cast (these instructions are still old)
    *   [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_cast_build_instructions.md)
    *   [Linux](https://chromium.googlesource.com/chromium/src/+/master/docs/linux_cast_build_instructions.md)
*   [Android](https://chromium.googlesource.com/chromium/src/+/master/docs/android_build_instructions.md)

### *The Chromium documentation is gradually moving into the source repository. The links above will take you there.*

## If you need help, try the [chromium-dev Google Group](https://groups.google.com/a/chromium.org/forum/#!forum/chromium-dev), or the #chromium IRC channel on irc.freenode.net (see the [IRC page](/developers/irc) for more on how Chromium uses IRC). **These are not support channels for Chrome itself but forums for developers.**

You can also, for a limited time, read [the old instructions for getting the
code](/developers/how-tos/old-get-the-code).