---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/how-tos
  - How-Tos
page_name: angle-infra
title: ANGLE Standalone Testing Infrastructure (obsolete)
---

This page is now moved to
<https://chromium.googlesource.com/angle/angle/+/master/infra/README.md>