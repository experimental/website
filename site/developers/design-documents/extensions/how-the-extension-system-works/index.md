---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/design-documents
  - Design Documents
- - /developers/design-documents/extensions
  - Extensions
page_name: how-the-extension-system-works
title: How the Extension System Works
---

<img alt="image" src="http://www.google.com/chart" height=300 width=500>