---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/design-documents
  - Design Documents
- - /developers/design-documents/extensions
  - Extensions
- - /developers/design-documents/extensions/proposed-changes
  - Proposed & Proposing New Changes
page_name: apiwishlist
title: API Wish List
---

An early design document containing an unsorted list of things we've want to do
as extension APIs. Some of these ideas have now been implemented.