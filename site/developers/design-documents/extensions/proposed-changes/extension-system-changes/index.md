---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/design-documents
  - Design Documents
- - /developers/design-documents/extensions
  - Extensions
- - /developers/design-documents/extensions/proposed-changes
  - Proposed & Proposing New Changes
page_name: extension-system-changes
title: Extension System
---

<img alt="image" src="http://www.google.com/chart" height=300 width=500>