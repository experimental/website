---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/design-documents
  - Design Documents
- - /developers/design-documents/accessibility
  - Accessibility Technical Documentation
page_name: tracker
title: Accessibility Issue Tracker
---

### Accessibility Issues

We use Chromium's issue tracker to keep track of bugs and feature development.
You can keep track of Accessibility issues using the following links:

[Open issues](http://code.google.com/p/chromium/issues/list) | [Fixed
issues](http://code.google.com/p/chromium/issues/list) | [All
issues](http://code.google.com/p/chromium/issues/list)