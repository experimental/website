---
breadcrumbs:
- - /developers
  - For Developers
- - /developers/design-documents
  - Design Documents
page_name: experiments
title: Experiments
---

Here's how to hide a feature behind a flag that can be changed from the
[chrome://flags](javascript:void(0);) page in Chrome, or with a command line
flag. We'll use an example from the following revision
[84971](http://src.chromium.org/viewvc/chrome) which hides the compact
navigation bar behind such a flag.

*   First, you need to add a string for your command line switch.
    For example:
    [chrome_switches.h](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/common/chrome_switches.h)
    &
    [chrome_switches.cc](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/common/chrome_switches.cc)
*   Then you need strings to be displayed on the
            [chrome://flags](javascript:void(0);) page for the name and
            description of your experiment.
    For example, the two strings with IDs starting with IDS_FLAGS_ENABLE in:
    [generated_resources.grd](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/app/generated_resources.grd)
*   And then add all this info to a new entry in the Experiments array.
    For example:
    [about_flags.cc](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/browser/about_flags.cc)
    *   The first string (e.g., "compact-navigation") is an identifier
                for the preference that will be saved in the user profile based
                on the settings chosen by the user on the
                [chrome://flags](javascript:void(0);) page.
    *   The the resource IDs for the name and description strings.
    *   Then which OS should expose it.
    *   And finally, the access to the command line flag, using one of
                the following macros:
    *   SINGLE_VALUE_TYPE_AND_VALUE, SINGLE_VALUE_TYPE, or
                MULTI_VALUE_TYPE.
*   And finally, use the
            CommandLine::ForCurrentProcess()-&gt;[HasSwitch](http://www.google.com/codesearch/p)(&lt;*your
            flag*&gt;) to check its state.
    For example:
    [tab_menu_model.cc](http://src.chromium.org/viewvc/chrome/trunk/src/chrome/browser/ui/tabs/tab_menu_model.cc)
    uses it to decide whether we show the Hide toolbar option in the tab
    contextual menu or not.