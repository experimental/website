---
breadcrumbs:
- - /developers
  - For Developers
page_name: web-development-style-guide
title: Web Development Style Guide
---

has moved to here:
<https://chromium.googlesource.com/chromium/src/+/master/styleguide/web/web.md>