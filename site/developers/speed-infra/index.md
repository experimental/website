---
breadcrumbs:
- - /developers
  - For Developers
page_name: speed-infra
title: Speed Infra
---

Content has moved to
<https://chromium.googlesource.com/chromium/src/+/master/docs/speed/README.md>