---
breadcrumbs:
- - /events
  - Chromium project related events.
page_name: blinkon-15
title: BlinkOn 15
---

**Save the (NEW) Dates!**

**Welcome to BlinkOn 15!** We have received a lot of feedback that after 1.6
years of working from home due to the pandemic, folks are feeling screen fatigue
from online meetings and events. Given this, we have adjusted our event down to
two days, Nov 16 & 17th. We are changing things up a bit and will host our event
on hopin, you can register [**here**](https://hopin.com/events/blinkon-15). We
will have the same format, however we will not have our Slack channels as all
networking can be done on the platform. You can also [download the hopin mobile
app](https://hopin.zendesk.com/hc/en-us/sections/360012637371--Mobile-App) on
your device.

## **Logistics**

*   **\[NEW\]** Dates: Nov 16-17, 2021 PST 9a - 12p PST
    *   Programming will start @ 9am PST each day, however you are
                welcome to join up to an hour earlier to find folks in chat, 1:1
                meetings and join our networking on the platform.
*   Location: Virtual

We continue to shift our BlinkOn events to more fairly distribute the burden of
meeting at challenging times. Next time we will prioritize the EMEA region. We
apologize this timing is not convenient for everyone, and to make this event as
accessible as possible, we'll publish all publicly available talks on our
[BlinkOn YouTube channel](https://www.youtube.com/user/blinkontalks) and within
the event under "Recordings" as soon as we are able. We hope that you understand
our decision and will join us live, or enjoy recordings on our BlinkOn Channel
when they become available.

## **Registration**

Save the \[NEW\] dates and [REGISTER NOW](https://hopin.com/events/blinkon-15)
for BlinkOn 15 - registration will remain open until one week before the start
of the event. We'll confirm all registrants and communicate more information
about the agenda.

## Lightning Talks

We have reached our limit and no longer accepting talks.

For our lightning talk speakers, thank you very much for signing up for our
BlinkOn 15 lightning talks!

*   All lightning talks need to be recorded
*   Please record a **~3 min** video and upload it to [the
            sheet](https://docs.google.com/spreadsheets/d/1FzxhGtmfaEP_yO8ePmWoYuvFS0qk44hQA5Mui06E9Rk/edit#gid=0)
            by **Nov 3!** (this is a hard deadline as our vendor will start
            processing the videos)

## Breakout Talks

## We have reached our limit and no longer accepting talks.

## \[WIP\] Schedule

## **<img alt="image" src="http://www.google.com/chart" height=700 width=475>**

## **Code of Conduct**

All attendees, speakers, sponsors, and volunteers at BlinkOn 15 are required to
agree with the following Code of Conduct. Organizers will enforce this code
throughout BlinkOn 15 to help ensure a safe environment for all attendees.

As developers and community organizers, we all pledge to respect everyone who
attends BlinkOn 15. We do not tolerate harassment of conference participants in
any form. Sexual language and imagery is not appropriate for any conference
venue, including talks, workshops, parties, Twitter, and other online media.
Communication must be constructive and never resort to personal attacks,
harassment, insults, or other unprofessional conduct. We promise to extend
courtesy and respect to all attendees regardless of gender, gender identity,
sexual orientation, disability, age, race, ethnicity, religion, or level of
experience. We expect all attendees to do the same. If any member of the
community violates this code of conduct, they may be sanctioned or expelled from
BlinkOn 15 and future BlinkOn events. If you are subject to or witness
unacceptable behavior, or have any other concerns, please email us at
[blinkon@chromium.org](mailto:blinkon@chromium.org) asap.

## Attendee & Speaker Best Practices in Hopin

*   [How to access an event as an
            attendee](https://assets-global.website-files.com/5fb2950be125da5aa25af58e/60b95a6bcaf8722b6b83752c_How%20to%20Access%20an%20Event%20as%20an%20Attendee%20(1).pdf)
*   [Being a speaker at a Hopin
            Event](https://assets-global.website-files.com/5fb2950be125da5aa25af58e/60b95bbb779c0790ddf0ae27_Best%20Practices%20for%20Speakers.pdf)
*   [How to use the Backstage as a
            speaker](https://hopin.zendesk.com/hc/en-us/articles/360056078472-How-to-use-the-Backstage-as-a-speaker)
*   [How to use the StreamYard Backstage as a
            speaker](https://hopin.zendesk.com/hc/en-us/articles/4406327583252-How-to-use-the-StreamYard-Backstage-as-a-speaker)
*   [Screensharing on
            backstage](https://hopin.zendesk.com/hc/en-us/articles/360056078412-How-to-present-slides-on-the-Hopin-Backstage)
*   [Sharing your
            PowerPoint](https://hopin.zendesk.com/hc/en-us/articles/360056527631-Sharing-your-Powerpoint)
*   [Speaker
            instructions](https://hopin.zendesk.com/hc/en-us/articles/360056078432-Speaker-Instructions)
*   [Tips for Speaking in and Moderating
            Sessions](https://hopin.zendesk.com/hc/en-us/articles/4402442259220-Tips-for-Speaking-in-and-Moderating-Sessions)

## **Organizer Information**

*   Planning committee:
            [blinkon@chromium.org](mailto:blinkon@chromium.org)