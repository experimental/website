---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: chrome-os-systems-supporting-linux
title: Chrome OS Systems Supporting Linux (Beta)
---

Linux (Beta), also known as Crostini, is a feature that lets you develop
software using your Chromebook. You can install Linux command line tools, code
editors, and IDEs on your Chromebook. These can be used to write code, create
apps, and more. Learn more on [The
Keyword](https://blog.google/products/chromebooks/linux-on-chromebooks/).
The Chromebooks, Chromeboxes, and Chromebases launched before 2019 that support
Linux (Beta) are listed below. Unless otherwise specified, all devices launched
in 2019 will support Linux (Beta).

To learn how to set up Linux (Beta) on supported Chromebooks, [click
here](https://support.google.com/chromebook/answer/9145439).

For in-depth technical documentation, [click
here](https://chromium.googlesource.com/chromiumos/docs/+/master/containers_and_vms.md).

<table>
<tr>

<td>Manufacturer</td>

<td>Device</td>

</tr>
<tr>

<td>Acer</td>

<td>Chromebook 11 (C732, C732T, C732L, C732LT)</td>

<td>Chromebook 11 (CB311-8H, CB311-8HT)</td>

<td>Chromebook 11 N7 (C731, C731T)</td>

<td>Chromebook 13 (CB713-1W)</td>

<td>Chromebook 14 (CB3-431)</td>

<td>Chromebook 15 (CB3-532)</td>

<td>Chromebook 15 (CB315-1H, CB315-1HT)</td>

<td>Chromebook 15 (CB515-1H, CB515-1HT)</td>

<td>Chromebook 514</td>

<td>Chromebook R11 (CB5-132T, C738T)</td>

<td>Chromebook R13 (CB5-312T)</td>

<td>Chromebook Spin 11 (CP311-H1, CP311-1HN)</td>

<td>Chromebook Spin 11 (R751T)</td>

<td>Chromebook Spin 13 (CP713-1WN)</td>

<td>Chromebook Spin 15 (CP315)</td>

<td>Chromebook Tab 10</td>

<td>Chromebox CXI3</td>

</tr>
<tr>

<td>ASUS</td>

<td>Chromebook C202SA</td>

<td>Chromebook C223</td>

<td>Chromebook C300SA / C301SA</td>

<td>Chromebook C423</td>

<td>Chromebook C523</td>

<td>Chromebook Flip C101PA</td>

<td>Chromebook Flip C213</td>

<td>Chromebox 3 (CN65)</td>

</tr>
<tr>

<td>CTL</td>

<td>Chromebook J41 / J41T</td>

<td>Chromebook NL7</td>

<td>Chromebook NL7 / NL7T-360 / NL7TW-360</td>

<td>Chromebook NL7 LTE</td>

<td>Chromebox CBx1</td>

<td>J5 Chromebook</td>

<td>NL61 Chromebook</td>

</tr>
<tr>

<td>Dell</td>

<td>Chromebook 11 (3180)</td>

<td>Chromebook 11 (5190)</td>

<td>Chromebook 11 2-in-1 (3189)</td>

<td>Chromebook 11 2-in-1 (5190)</td>

<td>Inspiron Chromebook 14 2-in-1 (7486)</td>

</tr>
<tr>

<td>Edugear</td>

<td>CMT Chromebook</td>

</tr>
<tr>

<td>Edxis</td>

<td>Education Chromebook (NL6D)</td>

</tr>
<tr>

<td>Google</td>

<td>Pixelbook</td>

<td>Pixel Slate</td>

<td>Pixelbook Go</td>

</tr>
<tr>

<td>Haier</td>

<td>Chromebook 11 C</td>

</tr>
<tr>

<td>HP</td>

<td>Chromebook 11 G5</td>

<td>Chromebook 11 G5 EE</td>

<td>Chromebook 11 G6 EE</td>

<td>Chromebook 14 G5</td>

<td>Chromebook 14A G5</td>

<td>Chromebook x2</td>

<td>Chromebook x360 11 G1 EE</td>

<td>Chromebook x360 14</td>

<td>Chromebox G2</td>

</tr>
<tr>

<td>Lenovo</td>

<td>100e Chromebook</td>

<td>300e/N23 Yoga/Flex 11 Chromebook</td>

<td>500e Chromebook</td>

<td>Flex 11 Chromebook</td>

<td>Ideapad C330 Chromebook</td>

<td>Ideapad S330 Chromebook</td>

<td>N22 Chromebook</td>

<td>N23 Chromebook</td>

<td>N23 Chromebook (Touch)</td>

<td>N42 Chromebook</td>

<td>ThinkPad 11e 3rd Gen Chromebook</td>

<td>ThinkPad 11e 4th Gen Chromebook</td>

<td>Yoga C630 Chromebook</td>

</tr>
<tr>

<td>Mecer</td>

<td>V2 Chromebook</td>

</tr>
<tr>

<td>Multilaser</td>

<td>Chromebook M11C</td>

</tr>
<tr>

<td>PCMerge</td>

<td>Chromebook PCM-116T-432B</td>

</tr>
<tr>

<td>Poin2</td>

<td>Chromebook 11C</td>

<td>Chromebook 14</td>

</tr>
<tr>

<td>Positivo</td>

<td>Chromebook C216B</td>

</tr>
<tr>

<td>Prowise</td>

<td>Chromebook Proline</td>

</tr>
<tr>

<td>Samsung</td>

<td>Chromebook 3</td>

<td>Chromebook Plus</td>

<td>Chromebook Plus (LTE)</td>

<td>Chromebook Plus (V2)</td>

</tr>
<tr>

<td>ViewSonic</td>

<td>NMP660 Chromebox</td>

</tr>
<tr>

<td>Viglen</td>

<td>Chromebook 360</td>

</tr>
</table>