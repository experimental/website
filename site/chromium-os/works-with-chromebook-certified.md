---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: works-with-chromebook-certified
title: Works With Chromebook Certified SKUs
---

Works With Chromebook accessories are tested and proven to meet Chromebook
compatibility standards. For more see the [Works With Chromebook
site](https://www.google.com/chromebook/workswithchromebook/).

Last Update: 13 October 2021

<table>
<tr>

<td>Dock / Hub</td>

</tr>
<tr>

<td>Targus</td>

<td><a href="https://us.targus.com/products/usb-c-to-4-port-usb-a-hub-ach226bt">USB-C to 4-Port USB-A Hub</a></td>

<td>ACH226BT, ACH226EU, ACH226CA</td>

</tr>
<tr>

<td>HP</td>

<td><a href="https://www.hp.com/us-en/shop/pdp/hp-usb-c-dock-g5">HP USB-C Dock G5</a></td>

<td>5TW10UT#ABA</td>

</tr>
<tr>

<td>Acer</td>

<td><a href="https://www.acer.com/ac/en/US/content/professional-model/GP.DCK11.00E">USB Type-C Dock D501</a></td>

<td>GP.DCK11.00E, GP.DCK11.00F, GP.DCK11.00G, GP.DCK11.00H</td>

</tr>
<tr>

<td>External Storage</td>

</tr>
<tr>

<td>Western Digital</td>

<td><a href="https://shop.westerndigital.com/products/portable-drives/wd-my-passport-usb-3-0-hdd">Western Digital My Passport 1TB</a></td>

<td>WDBPKJ0040BBK, WDBYVG0010BBK-WESN, WDBYVG0010BBK-CESN, WDBYVG0010BBL-WESN, WDBYVG0010BBL-CESN, WDBYVG0010BRD-WESN, WDBYVG0010BRD-CESN, WDBYVG0010BWT-WESN, WDBYVG0010BWT-CESN, WDBYVG0020BBK-WESN, WDBYVG0020BBK-CESN, WDBYVG0020BBL-WESN, WDBYVG0020BBL-CESN, WDBYVG0020BRD-WESN, WDBYVG0020BRD-CESN, WDBYVG0020BWT-WESN, WDBYVG0020BWT-CESN, WDBPKJ0050BBK-WESN, WDBPKJ0050BBK-CESN, WDBPKJ0050BBL-WESN, WDBPKJ0050BBL-CESN, WDBPKJ0050BRD-WESN, WDBPKJ0050BRD-CESN, WDBPKJ0050BWT-WESN, WDBPKJ0050BWT-CESNN</td>

</tr>
<tr>

<td>Western Digital</td>

<td><a href="https://www.walmart.com/ip/WD-2TB-My-Passport-Portable-Hard-Drive/763672545">WD Drive for Chromebook</a> (Walmart only)</td>

<td>WDBB7B0020BBL-WESN</td>

</tr>
<tr>

<td>Western Digital</td>

<td>200GB SanDisk Ultra PLUS UHS-I microSD</td>

<td>SDSQUB3-200G-AW6MA</td>

</tr>
<tr>

<td>Western Digital</td>

<td>128GB SanDisk Ultra PLUS UHS-I microSD</td>

<td>SDSQUB3-128G-AN6TN</td>

</tr>
<tr>

<td>Western Digital</td>

<td>64GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-064G</td>

</tr>
<tr>

<td>Western Digital</td>

<td>128GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-128G, SDSQUAR-128G-GN6MA</td>

</tr>
<tr>

<td>Western Digital</td>

<td>256GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-256G</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/GXB0Z91295">F510 Hard Disk Drive</a></td>

<td>GXB0Z91290, GXB0Z91295, GXB0Z91294</td>

</tr>
<tr>

<td>Headset (corded)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/dk/enterprise/products/sc-230-usb-headset-504403">IMPACT SC 230 USB ☊</a></td>

<td>504403 (1000516)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/dk/enterprise/products/sc-260-headset-504402">IMPACT SC 260 USB ☊</a></td>

<td>504404 (1000517)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/gaming/products/pc-8.2-usb-voice-over-ip-headset-508375">PC 8.2 USB</a></td>

<td>508375</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/pk/enterprise/products/adapt-135-ii-headset-1000907">ADAPT 135 II ☊</a></td>

<td>1000907</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/enterprise/products/adapt-165-ii-headset-1000908">ADAPT 165 II ☊</a></td>

<td>1000908</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/gaming/products/pc-5-chat-voice-over-ip-headset-508328">PC 5 CHAT</a></td>

<td>508328</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/enterprise/products/edu-10-headset-1001109">EDU 10</a></td>

<td>1000109</td>

</tr>
<tr>

<td>Jabra</td>

<td><a href="https://www.jabra.com.au/business/contact-center-headsets/jabra-biz-1100-edu">Biz 1100 EDU 3.5mm</a></td>

<td>1159-0139-EDU</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD0X88524">Stereo USB Headset</a></td>

<td>4XD0X88524 / GXD1B67867</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD0K25030">Essential Stereo Analog Headset</a></td>

<td>4XD0K25030 / GXD1B60597</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD1B61617">Mono USB Headset</a></td>

<td>4XD1B61617</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/usb-headset-h340">H340 USB Computer Headset</a></td>

<td>981-000507</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/usb-headset-h540">H540 USB Computer Headset ☊</a></td>

<td>981-000510</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/h370-usb-headset-noise-canceling-mic">H370 USB Computer Headset</a></td>

<td>981-000710</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/stereo-headset-h111">H111 Stereo Headset</a></td>

<td>981-000612</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/stereo-headset-h390">H390 USB Computer Headset ☊</a></td>

<td>981-000014</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-3300">Blackwire 3315T - 3.5mm Top Only (Monaural)</a></td>

<td>216898-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-3300">Blackwire 3325T - 3.5mm Top Only (Binaural)</a></td>

<td>216899-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-5200">Blackwire C5210T - 3.5mm Top Only (Monaural) ☊</a></td>

<td>211008-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-5200">Blackwire C5220T - 3.5mm Top Only (Binaural) ☊</a></td>

<td>211008-02</td>

</tr>
<tr>

<td>Steel Series</td>

<td><a href="https://steelseries.com/gaming-headsets/arctis-3">Arctis 3</a></td>

<td>61503</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/HS-HP27UBK.html">Overhead USB Headset (USB-A)</a></td>

<td>HS-HP27UBK</td>

</tr>
</table>
<table>
<tr>

<td>Dock / Hub</td>

</tr>
<tr>

<td>Targus</td>

<td><a href="https://us.targus.com/products/usb-c-to-4-port-usb-a-hub-ach226bt">USB-C to 4-Port USB-A Hub</a></td>

<td>ACH226BT, ACH226EU, ACH226CA</td>

</tr>
<tr>

<td>HP</td>

<td><a href="https://www.hp.com/us-en/shop/pdp/hp-usb-c-dock-g5">HP USB-C Dock G5</a></td>

<td>5TW10UT#ABA</td>

</tr>
<tr>

<td>Acer</td>

<td><a href="https://www.acer.com/ac/en/US/content/professional-model/GP.DCK11.00E">USB Type-C Dock D501</a></td>

<td>GP.DCK11.00E, GP.DCK11.00F, GP.DCK11.00G, GP.DCK11.00H</td>

</tr>
<tr>

<td>External Storage</td>

</tr>
<tr>

<td>Western Digital</td>

<td><a href="https://shop.westerndigital.com/products/portable-drives/wd-my-passport-usb-3-0-hdd">Western Digital My Passport 1TB</a></td>

<td>WDBPKJ0040BBK, WDBYVG0010BBK-WESN, WDBYVG0010BBK-CESN, WDBYVG0010BBL-WESN, WDBYVG0010BBL-CESN, WDBYVG0010BRD-WESN, WDBYVG0010BRD-CESN, WDBYVG0010BWT-WESN, WDBYVG0010BWT-CESN, WDBYVG0020BBK-WESN, WDBYVG0020BBK-CESN, WDBYVG0020BBL-WESN, WDBYVG0020BBL-CESN, WDBYVG0020BRD-WESN, WDBYVG0020BRD-CESN, WDBYVG0020BWT-WESN, WDBYVG0020BWT-CESN, WDBPKJ0050BBK-WESN, WDBPKJ0050BBK-CESN, WDBPKJ0050BBL-WESN, WDBPKJ0050BBL-CESN, WDBPKJ0050BRD-WESN, WDBPKJ0050BRD-CESN, WDBPKJ0050BWT-WESN, WDBPKJ0050BWT-CESNN</td>

</tr>
<tr>

<td>Western Digital</td>

<td><a href="https://www.walmart.com/ip/WD-2TB-My-Passport-Portable-Hard-Drive/763672545">WD Drive for Chromebook</a> (Walmart only)</td>

<td>WDBB7B0020BBL-WESN</td>

</tr>
<tr>

<td>Western Digital</td>

<td>200GB SanDisk Ultra PLUS UHS-I microSD</td>

<td>SDSQUB3-200G-AW6MA</td>

</tr>
<tr>

<td>Western Digital</td>

<td>128GB SanDisk Ultra PLUS UHS-I microSD</td>

<td>SDSQUB3-128G-AN6TN</td>

</tr>
<tr>

<td>Western Digital</td>

<td>64GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-064G</td>

</tr>
<tr>

<td>Western Digital</td>

<td>128GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-128G, SDSQUAR-128G-GN6MA</td>

</tr>
<tr>

<td>Western Digital</td>

<td>256GB SanDisk Ultra UHS-I microSD</td>

<td>SDSQUA4-256G</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/GXB0Z91295">F510 Hard Disk Drive</a></td>

<td>GXB0Z91290, GXB0Z91295, GXB0Z91294</td>

</tr>
<tr>

<td>Headset (corded)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/dk/enterprise/products/sc-230-usb-headset-504403">IMPACT SC 230 USB ☊</a></td>

<td>504403 (1000516)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/dk/enterprise/products/sc-260-headset-504402">IMPACT SC 260 USB ☊</a></td>

<td>504404 (1000517)</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/gaming/products/pc-8.2-usb-voice-over-ip-headset-508375">PC 8.2 USB</a></td>

<td>508375</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/pk/enterprise/products/adapt-135-ii-headset-1000907">ADAPT 135 II ☊</a></td>

<td>1000907</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/enterprise/products/adapt-165-ii-headset-1000908">ADAPT 165 II ☊</a></td>

<td>1000908</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/gaming/products/pc-5-chat-voice-over-ip-headset-508328">PC 5 CHAT</a></td>

<td>508328</td>

</tr>
<tr>

<td>EPOS</td>

<td><a href="https://www.eposaudio.com/en/us/enterprise/products/edu-10-headset-1001109">EDU 10</a></td>

<td>1000109</td>

</tr>
<tr>

<td>Jabra</td>

<td><a href="https://www.jabra.com.au/business/contact-center-headsets/jabra-biz-1100-edu">Biz 1100 EDU 3.5mm</a></td>

<td>1159-0139-EDU</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD0X88524">Stereo USB Headset</a></td>

<td>4XD0X88524 / GXD1B67867</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD0K25030">Essential Stereo Analog Headset</a></td>

<td>4XD0K25030 / GXD1B60597</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4XD1B61617">Mono USB Headset</a></td>

<td>4XD1B61617</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/usb-headset-h340">H340 USB Computer Headset</a></td>

<td>981-000507</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/usb-headset-h540">H540 USB Computer Headset ☊</a></td>

<td>981-000510</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/h370-usb-headset-noise-canceling-mic">H370 USB Computer Headset</a></td>

<td>981-000710</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/stereo-headset-h111">H111 Stereo Headset</a></td>

<td>981-000612</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/stereo-headset-h390">H390 USB Computer Headset ☊</a></td>

<td>981-000014</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-3300">Blackwire 3315T - 3.5mm Top Only (Monaural)</a></td>

<td>216898-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-3300">Blackwire 3325T - 3.5mm Top Only (Binaural)</a></td>

<td>216899-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-5200">Blackwire C5210T - 3.5mm Top Only (Monaural) ☊</a></td>

<td>211008-01</td>

</tr>
<tr>

<td>Poly</td>

<td><a href="https://www.poly.com/us/en/products/headsets/blackwire/blackwire-5200">Blackwire C5220T - 3.5mm Top Only (Binaural) ☊</a></td>

<td>211008-02</td>

</tr>
<tr>

<td>Steel Series</td>

<td><a href="https://steelseries.com/gaming-headsets/arctis-3">Arctis 3</a></td>

<td>61503</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/HS-HP27UBK.html">Overhead USB Headset (USB-A)</a></td>

<td>HS-HP27UBK</td>

</tr>
</table>
<table>
<tr>

<td>Mouse (corded)</td>

</tr>
<tr>

<td>Kensington</td>

<td><a href="https://www.kensington.com/p/products/electronic-control-solutions/computer-mice-/wired-mouse-for-life-certified-by-works-with-chromebook/">mouse for Life Corded</a></td>

<td>K72110US</td>

</tr>
<tr>

<td>Kensington</td>

<td><a href="https://www.kensington.com/p/products/electronic-control-solutions/computer-mice-/mouseinabox-usb-certified-by-works-with-chromebook/">mouse-in-a-Box Wired USB</a></td>

<td>K72356US</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://www.lenovo.com/us/en/accessories-and-monitors/keyboards-and-mice/mice/MICE-BO-Lenovo-Essential-USB-Mouse/p/4Y50R20863">Lenovo Essential USB mouse, 300 USB mou</a>se</td>

<td>4Y50R20863, 4Y50T44377, GX30M39704</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/mouse-m100">B100/M100/M100R Corded mouse (Black)</a></td>

<td>910-001439</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-K5URBKRS.html">USB optical mouse (S,M)</a></td>

<td>M-K5URBK /RS, M-K6URBK /RS</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-S2ULBKRS.html">USB Laser mouse (M)</a></td>

<td>M-S2ULBK/RS</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-K7URBKRS.html">Wired 3 buttons Red LED mouse</a></td>

<td>M-K7URBK/RS</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-Y8UBBK.html">BlueLED 3 buttons wired mouse</a></td>

<td>M-Y8UBBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-XGS10UBBK.html">EX-G wired BlueLED mouse (S,M)</a></td>

<td>M-XGS10UBBK, M-XGM10UBBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-XGM10DBBK.html">EX-G wireless BlueLED mouse (M, L)</a></td>

<td>M-XGM10DBBK, M-XGL10UBBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/M-XGS10UBSBK.html">Silent EX-G wired BlueLED mouse (S,M,L)</a></td>

<td>M-XGS10UBSBK, M-XGM10UBSBK, M-XGL10UBSBK</td>

</tr>
<tr>

<td>Printer</td>

</tr>
<tr>

<td>Brother</td>

<td><a href="https://www.brother-usa.com/products/hll2350dw">HL-L2350DW</a></td>

<td>HL-L2350DW</td>

</tr>
<tr>

<td>Brother</td>

<td><a href="https://www.brother-usa.com/products/hll2370dw">HL-L2370DW</a></td>

<td>HL-L2370DW</td>

</tr>
<tr>

<td>Brother</td>

<td><a href="https://www.brother.co.jp/product/printer/laserprinter/hll2375dw/index.aspx">HL-L2375DW</a></td>

<td>HL-L2375DW</td>

</tr>
<tr>

<td>Brother</td>

<td><a href="https://www.brother.co.jp/product/printer/laserprinter/hll3230cdw/index.aspx">HL-L3230CDW</a></td>

<td>HL-L3230CDW</td>

</tr>
<tr>

<td>USI Stylus/Pen</td>

</tr>
<tr>

<td>ALOGIC</td>

<td><a href="https://www.alogic.co/us/alogic-active-stylus-pen.html">ALOGIC USI Active Stylus Pen</a></td>

<td>ALUS19</td>

</tr>
<tr>

<td>HP</td>

<td><a href="https://store.hp.com/us/en/pdp/hp-rechargable-usi-pen">HP USI Active Stylus</a></td>

<td>8NN78AA#ABL</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://www.lenovo.com/us/en/accessories-and-monitors/stylus-pens-and-supplies/pens/TAB-ACC-BO-Lenovo-USI-Pen/p/4X80Z49662">USI Pen</a></td>

<td>4X80Z49662, GX81B10212</td>

</tr>
<tr>

<td>Targus</td>

<td><a href="https://us.targus.com/products/active-stylus-for-chromebook-amm173gl">Active Stylus for Chromebook</a></td>

<td>AMM173GL</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/P-TPUSI01BK.html">USI Stylus</a></td>

<td>P-TPUSI0BK</td>

</tr>
<tr>

<td>Webcam</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/products/webcams/c505-hd-webcam.html">C505e HD webcam</a></td>

<td>960-001385</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/hd-webcam-c270">C270 HD webcam</a></td>

<td>960-000694</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/hd-pro-webcam-c920">C920 HD Pro webcam</a></td>

<td>960-000764</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/hd-webcam-c310">C310 HD webcam</a></td>

<td>960-000585</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/product/c930e-webcam">C930E Business webcam</a></td>

<td>960-000971</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.amazon.com/Logitech-C920x-Pro-HD-Webcam/dp/B085TFF7M1">C920x HD Pro webcam</a></td>

<td>960-001335</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/products/webcams/c920s-pro-hd-webcam.html">C920s Pro HD webcam</a></td>

<td>960-001257</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitech.com/en-us/products/webcams/c505-hd-webcam.html">C505</a></td>

<td>960-001363</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/UCAM-C310FBBK.html">1 million Pixels webcamera</a></td>

<td>UCAM-C310FBBK</td>

</tr>
</table>

<table>
<tr>

<td>Pen Tablet</td>

</tr>
<tr>

<td>Wacom</td>

<td><a href="https://www.wacom.com/en-us/products/pen-tablets/one-by-wacom">One by Wacom (small, medium)</a></td>

<td>CTL-472, CTL-672</td>

</tr>
<tr>

<td>Wacom</td>

<td><a href="https://www.wacom.com/en-us/products/pen-tablets/wacom-intuos">Wacom Intuos (small, medium)</a></td>

<td>CTL-4100, CTL-6100</td>

</tr>
<tr>

<td>Wacom</td>

<td><a href="https://www.wacom.com/en-us/products/pen-tablets/wacom-intuos">Wacom Intuos with Bluetooth (small, medium)</a></td>

<td>CTL-4100WL, CTL-6100WL</td>

</tr>
<tr>

<td>Wall Charger</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.amazon.com/AK-A2017121-27W-PD-1-Port/dp/B07DFGKFM3">PowerPort Atom 30W Charger</a></td>

<td>A2017</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.amazon.com/Anker-Charger-PowerPort-Powerline-Delivery/dp/B07Q8SHX5D">PowerPort Atom 30W Charger/Cable Bundle</a></td>

<td>B2017</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.amazon.com/Anker-Powerline-Certified-Delivery-Chromebook/dp/B07RN3KCC6">PowerLine II 6ft C-C cable</a></td>

<td>A8482</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.target.com/p/anker-powerport-atom-iii-slim-30w-charger-with-6-39-c-c-cable-white/-/A-82676113#lnk=sametab">PowerPort Atom III Slim 30W Charger with 6' C-C Cable</a></td>

<td>B2618</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/ACDC-PD0145BK.html">USB Type-C PD45w AC Adapter</a></td>

<td>ACDC-PD0145BK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/ACDC-PD0465BK.html">USB Type-C PD65w AC Adapter</a></td>

<td>ACDC-PD0465BK</td>

</tr>
<tr>

<td>HP</td>

<td><a href="https://store.hp.com/us/en/pdp/hp-45w-usb-c-ac-adapter">HP 45W USB-C AC Adapter</a></td>

<td>N8N14AA#ABL</td>

</tr>
<tr>

<td>HP</td>

<td><a href="https://store.hp.com/sg-en/default/hp-usb-c-travel-power-adapter-65w-x7w50aa.html">HP Travel USB-C AC Adapter 65W</a></td>

<td>X7W50AA#ABL</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-WCH002/">USB-C GaN Wall Charger 60W</a></td>

<td>WCH002dqWH</td>

</tr>
<tr>

<td>Gaming Controller</td>

</tr>
<tr>

<td>Google</td>

<td><a href="https://store.google.com/us/product/stadia_controller">Stadia Controller (H2B)</a></td>

<td>6428982</td>

</tr>
<tr>

<td>Steel Series</td>

<td><a href="https://steelseries.com/gaming-controllers/stratus-duo">Stratus Duo Controller</a></td>

<td>69075</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitechg.com/en-us/products/gamepads/f710-wireless-gamepad.html">F710 Gamepad</a></td>

<td>940-000117</td>

</tr>
<tr>

<td>Logitech</td>

<td><a href="https://www.logitechg.com/en-us/products/gamepads/f310-gamepad.html">F310 Gamepad</a></td>

<td>940-000110</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/JC-U4113SBK.html">Gamepad wireless Black 293</a></td>

<td>JC-U4113SBK</td>

</tr>
<tr>

<td>Adapter</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU038bt/">USB Type C to HDMI External Adapter</a></td>

<td>F2CU038btBLK</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU037/">USB Type C to VGA Adapter</a></td>

<td>F2CU037btBLK</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU036/">USB 3.0, TYPE C-USB Adapter</a></td>

<td>F2CU036btBLK</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU038bt/">USB Type C to HDMI External Adapter</a></td>

<td>F2CU038btBLK-MG</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU037/">USB Type C to VGA Adapter</a></td>

<td>F2CU037btBLK-MG</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-F2CU036/">USB 3.0, TYPE C-USB Adapter</a></td>

<td>F2CU036btBLK-MG</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN7RT8W">USB-C Ethernet Adapter</a></td>

<td>201013-BLK</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN6W6TY">USB-C Multiport Adapter with 4K 60Hz DisplayPort</a></td>

<td>201246-GRY</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN81KP8">USB-C Multiport Adapter with 4K 60Hz HDMI</a></td>

<td>201248-GRY</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN7RT8W">USB-C Ethernet Adapter</a></td>

<td>201213-GRY</td>

</tr>
<tr>

<td>J5 Create</td>

<td><a href="https://en.j5create.com/products/jucx05">JUCX05 USB Type-C™ 3.1 to Type-A Adapter</a></td>

<td>JUCX05</td>

</tr>
<tr>

<td>J5Create</td>

<td><a href="https://en.j5create.com/products/jca153g">USB™ Type-C to 4K HDMI™ Adapter</a></td>

<td>JCA153G</td>

</tr>
<tr>

<td>Kensington</td>

<td><a href="https://www.kensington.com/p/products/device-docking-connectivity-products/video-cables-adapters/cv4000h-usb-c-4k-hdmi-adapter-certified-by-works-with-chromebook/">USB-C 4K HDMI Adapter</a></td>

<td>K33993WW</td>

</tr>
<tr>

<td>Kensington</td>

<td><a href="https://www.kensington.com/p/products/device-docking-connectivity-products/video-cables-adapters/cv2000v-usb-c-hd-vga-adapter-certified-by-works-with-chromebook/">USB-C HD VGA Adapter</a></td>

<td>K33994WW</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.startech.com/USB31000SA">USB 3.0 to Gigabit Network Adapter - Silver</a></td>

<td>USB31000SA</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.startech.com/Cables/usb-c/usb-3-0-c-a-adapter-cable~USB31CAADP">USB-C to USB-A Adapter Cable - M/F - 6in - USB 3.0 </a></td>

<td>USB31CAADP</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4X90R61022">Lenovo USB-C to HDMI 2.0b Adapter</a></td>

<td>4X90R61022 (GX90R61025, GX90R61026)</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/GX90M44578">Lenovo USB-C to VGA Adapter</a></td>

<td>4X90M42956 (GX90M44574, GX90M44575, GX90M44578, GX90M44580)</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-AFCM01NBK.html">USB3.1Gen1 0.15m C-to-A Adapter</a></td>

<td>USB3-AFCM01NBK</td>

</tr>
</table>
<table>
<tr>

<td>Cable</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.anker.com/products/variant/anker-powerline--iii-usb-c-to-usb-c-6ft/A8863011">PowerLine +3 C-C 3ft cable 2.0</a></td>

<td>A8862</td>

</tr>
<tr>

<td>Anker</td>

<td><a href="https://www.anker.com/products/variant/anker-powerline--iii-usb-c-to-usb-c-6ft/A8863011">PowerLine +3 C-C 6ft cable 2.0</a></td>

<td>A8863</td>

</tr>
<tr>

<td>Acer</td>

<td><a href="https://www.amazon.com/dp/B08C1GD91Q">Type C (Gen 2) Cable with 1M length</a></td>

<td>GP.DNG11.005</td>

</tr>
<tr>

<td>ALOGIC</td>

<td><a href="https://www.alogic.co/us/catalog/product/view/id/4143/s/alogic-elements-pro-usb-2-0-usb-c-to-usb-c-cable-1m-black-5a-480mbps/">PRO USB-C to USB-C Cable (USB 2.0), 1m</a></td>

<td>ELPCC201-BK, ELPCC201-WH</td>

</tr>
<tr>

<td>ALOGIC</td>

<td><a href="https://www.alogic.co/us/catalog/product/view/id/4143/s/alogic-elements-pro-usb-2-0-usb-c-to-usb-c-cable-1m-black-5a-480mbps/">PRO USB-C to USB-C Cable (USB 2.0), 2m, Black</a></td>

<td>ELPCC202-BK, ELPCC202-WH</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-CAB002/">Braided USB-C to USB-A Cable 6.6 ft</a></td>

<td>CAB002bt2MBK</td>

</tr>
<tr>

<td>Belkin</td>

<td><a href="https://www.belkin.com/us/p/P-CAB001/">USB-C to USB-A Cable 6.6 ft</a></td>

<td>CAB001bt2MBK</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B0815BZSGW">USB-C 3.1 Gen 1 Cable (PD 2.0), 6 Foot</a></td>

<td>201245-GRY-6</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN74KZT">USB-C to DisplayPort Cable</a></td>

<td>201236-GRY-6</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YN7RHGT">USB-C to HDMI Cable</a></td>

<td>201262-GRY-6</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B07YQ66K42">USB-C to Micro USB 3.1 Gen 2 Cable</a></td>

<td>201206-GRY-1.5</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="http://www.amazon.com/dp/B0815PHMN9">USB-C 3.1 Gen 2 Cable (PD 3.0)</a></td>

<td>201235-GRY.1m</td>

</tr>
<tr>

<td>Cable Matters</td>

<td><a href="https://www.amazon.com/Cable-Matters-USB-IF-Certified-Black/dp/B06XMY49BM">10 Gbps Gen 2 USB A - USB C Cable (USB C to USB Cable)</a></td>

<td>201041-BLK-1m</td>

</tr>
<tr>

<td>J5Create</td>

<td><a href="https://www.amazon.com/Cable-Matters-USB-IF-Certified-Black/dp/B06XMY49BM">10 Gbps Gen 2 USB A - USB C Cable (USB C to USB Cable)</a></td>

<td>JUP2290</td>

</tr>
<tr>

<td>J5Create</td>

<td><a href="https://en.j5create.com/products/jcc153g">USB-C™ to 4K HDMI™ Cable</a></td>

<td>JCC153G</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4X90Q93303">USB-C to DP</a></td>

<td>4X90Q93303 (GX90Q93306, GX90Q93307, GX90Q93308)</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4X90M42956">USB-C to VGA</a></td>

<td>4X90M42956 (GX90M44574, GX90M44575, GX90M44578, GX90M44580)</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://accessorysmartfind.lenovo.com/#/products/4X90S91831">USB-C to Ethernet</a></td>

<td>4X90S91831</td>

</tr>
<tr>

<td>Lenovo</td>

<td><a href="https://www.lenovo.com/us/en/accessories-and-monitors/cables-and-adapters/cables/CABLE-BO-USB-C-to-USB-C-Cable-2m/p/4X90Q59480">3a Gen1 C-C USB-C Cable (2m)</a></td>

<td>4X90Q59480</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.startech.com/Cables/usb-c/usb-c-usb-3-cable-power-delivery~USB315C5C6">USB-C cable 1.8m/6ft with Power Delivery 5A (5Gbps)</a></td>

<td>USB315C5C6</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.startech.com/Cables/usb-c/usb-3-1-a-c-cable~USB31AC1M">USB3 Gen2 1m A-C 3a cable</a></td>

<td>USB31AC1M</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.amazon.com/StarTech-com-DisplayPort-1-4-Cable-VESA-Certified/dp/B07KCZ6L4L">6.5ft. (2 m) DisplayPort 1.4 Cable - VESA® Certified</a></td>

<td>DP14MM2M</td>

</tr>
<tr>

<td>Startech</td>

<td><a href="https://www.startech.com/Cables/Audio-Video/HDMI/2-m-durable-high-speed-hdmi-cable~RHDMM2MP">2 m (6.6 ft.) Premium High Speed HDMI Cable with Ethernet - 4K 60Hz</a></td>

<td>RHDMM2MP</td>

</tr>
</table>
<table>
<tr>

<td>Cable</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P30NBK.html">USB2.0 3.0m C-C 5a</a></td>

<td>U2C-CC5P30NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P40NBK.html">USB2.0 4.0m C-C 5a</a></td>

<td>U2C-CC5P40NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P20NBK.html">USB2.0 2.0m C-C 5a</a></td>

<td>U2C-CC5P20NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P15NBK.html">USB2.0 1.5m C-C 5a</a></td>

<td>U2C-CC5P15NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P10NBK.html">USB2.0 1.0m C-C 5a</a></td>

<td>U2C-CC5P10NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-CC5P05NBK.html">USB2.0 0.5m C-C 5a</a></td>

<td>U2C-CC5P05NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-CC5P10NBK.html">USB3.1Gen2 1.0m C-C 5a</a></td>

<td>USB3-CC5P10NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-CC5P05NBK.html">USB3.1Gen2 0.5m C-C 5a</a></td>

<td>USB3-CC5P05NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-CCP05NBK.html">USB3.1Gen2 0.5m C-C 3a 619</a></td>

<td>USB3-CCP05NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-AC05NBK.html">USB3.1Gen2 0.5m A-C 3a 718</a></td>

<td>USB3-AC05NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-AC10NBK.html">USB3.1Gen2 1.0m A-C 3a 725</a></td>

<td>USB3-AC10NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC30NBK.html">USB2.0 3.0m A-C 3a 329</a></td>

<td>U2C-AC30NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC20NBK.html">USB2.0 2.0m A-C 3a 312</a></td>

<td>U2C-AC20NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC10NBK.html">USB2.0 1.0m A-C 3a 299</a></td>

<td>U2C-AC10NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC15NBK.html">USB2.0 1.5m A-C 3a 305</a></td>

<td>U2C-AC15NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC40NBK.html">USB2.0 4.0m A-C 3a 336</a></td>

<td>U2C-AC40NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/U2C-AC05NBK.html">USB2.0 0.5m A-C 3a 282</a></td>

<td>U2C-AC05NBK</td>

</tr>
<tr>

<td>Elecom</td>

<td><a href="https://www.elecom.co.jp/products/USB3-CCP10NBK.html">USB3.1Gen2 1.0m C-C 3a 629</a></td>

<td>USB3-CCP10NBK</td>

</tr>
</table>

Some products will have a dependency - like a USB-C port - in order to work
seamlessly. Please check the product packaging for more information.

Google is neither responsible for the operation of the products nor their
compliance with any applicable or other safety requirements. Products are
serviced and supported exclusively by the accessory or peripheral manufacturer
in accordance with the terms and conditions of the product. Chromebook and the
"Works With Chromebook" badge are trademarks of Google LLC.