---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: ccd
title: Closed Case Debugging (CCD)
---

## This page has moved to <https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/master/docs/ccd.md>. Please update the link that brought you here.