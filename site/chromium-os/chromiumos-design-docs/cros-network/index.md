---
breadcrumbs:
- - /chromium-os
  - Chromium OS
- - /chromium-os/chromiumos-design-docs
  - Design Documents
page_name: cros-network
title: 'CrOS network: notes on ChromiumOS networking'
---

<img alt="image" src="http://www.google.com/chart" height=300 width=500>