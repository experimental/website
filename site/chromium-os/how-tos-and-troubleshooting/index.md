---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: how-tos-and-troubleshooting
title: How Tos and Troubleshooting
---

The pages in this section have practical instructions to help you use and modify
a Chromium-based OS. Also see [Chromium OS Developer
Guide](/chromium-os/developer-guide).

<img alt="image" src="http://www.google.com/chart" height=300 width=500>