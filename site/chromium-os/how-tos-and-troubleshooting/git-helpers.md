---
breadcrumbs:
- - /chromium-os
  - Chromium OS
- - /chromium-os/how-tos-and-troubleshooting
  - How Tos and Troubleshooting
page_name: git-helpers
title: Git documentation elsewhere
---

## Tech Talks

*   Randall Schwartz's [Tech Talk on git](http://www.youtube.com/watch)
*   [gitcasts](http://www.google.com/url)

## Online References

*   [Git Magic](http://www.google.com/url)
*   [Git for computer scientists](http://www.google.com/url)
*   [Git Manual](http://www.google.com/url)
*   [git-scm.](http://www.google.com/url)[com](http://www.google.com/url)
*   [Git Visual Guide](http://marklodato.github.com/visual-git-guide/)

## Online Books

*   [Pro Git](http://www.google.com/url)
*   [Git Community Book](http://www.google.com/url)