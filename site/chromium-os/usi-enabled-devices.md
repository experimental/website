---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: usi-enabled-devices
title: USI Capable Devices
---

<table>
<tr>
<td>Acer Chromebook Spin 13 (CP713-1WN)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 314 (CP314-1HN)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 511 (R753TM/R753TNM)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 512 (R853TA) & Acer Chromebook spin 512 (R853TNA)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 513 (CP513-1H、CP513-1HL、R841T、R841LT)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 514 (CP514-2H CP514-1H、CP514-1HH、CP514-2H、CP514-1W、CP514-1W)</td>
</tr>
<tr>
<td>Acer Chromebook Spin 713 (CP713-2W、CP713-3W)</td>
</tr>
<tr>
<td>Acer Chromebook Tab 10</td>
</tr>
<tr>
<td>ASUS Chromebook C424</td>
</tr>
<tr>
<td>ASUS Chromebook Detachable CM3</td>
</tr>
<tr>
<td>ASUS Chromebook Detachable CZ1</td>
</tr>
<tr>
<td>ASUS Chromebook Flip C213</td>
</tr>
<tr>
<td>ASUS Chromebook Flip C214 (EDU), ASUS Chromebook Flip C234 (CANADA CONSUMER)</td>
</tr>
<tr>
<td>ASUS Chromebook Flip C436FA</td>
</tr>
<tr>
<td>ASUS Chromebook Flip CM3</td>
</tr>
<tr>
<td>ASUS Chromebook Flip CR1100FKA</td>
</tr>
<tr>
<td>ASUS Chromebook Flip CX3</td>
</tr>
<tr>
<td>ASUS Chromebook Flip CX5 (CX5400)</td>
</tr>
<tr>
<td>ASUS Chromebook Flip CX5 (CX5500)</td>
</tr>
<tr>
<td>Dell Chromebook 11 2-in-1 (5190)</td>
</tr>
<tr>
<td>Dell Chromebook 3110 2in1</td>
</tr>
<tr>
<td>Dell Inspiron Chromebook 14 2-in-1 (7486)</td>
</tr>
<tr>
<td>Dell Latitude 7410 Chromebook Enterprise</td>
</tr>
<tr>
<td>Google Pixel Slate</td>
</tr>
<tr>
<td>Google Pixelbook</td>
</tr>
<tr>
<td>HP Chromebook x2</td>
</tr>
<tr>
<td>HP Chromebook x2 11c</td>
</tr>
<tr>
<td>HP Chromebook x360 11 G4 EE</td>
</tr>
<tr>
<td>HP Chromebook x360 12b</td>
</tr>
<tr>
<td>HP Elite c1030 Chromebook / HP Chromebook x360 13c</td>
</tr>
<tr>
<td>IdeaPad Flex 5i Chromebook (13", 5)</td>
</tr>
<tr>
<td>IdeaPad Flex 5i Chromebook (13", 6)</td>
</tr>
<tr>
<td>Lenovo 300e Chromebook Gen 3</td>
</tr>
<tr>
<td>Lenovo 300e/500e Chromebook 2nd Gen</td>
</tr>
<tr>
<td>Lenovo 500e Chromebook</td>
</tr>
<tr>
<td>Lenovo 500e Chromebook Gen 3</td>
</tr>
<tr>
<td>Lenovo Chromebook Duet 5 / IdeaPad Duet 5 Chromebook</td>
</tr>
<tr>
<td>Samsung Chromebook Plus</td>
</tr>
<tr>
<td>Samsung Chromebook Plus (V2)</td>
</tr>
<tr>
<td>Samsung Chromebook Pro</td>
</tr>
<tr>
<td>Samsung Galaxy Chromebook</td>
</tr>
<tr>
<td>Samsung Galaxy Chromebook Go 360</td>
</tr>
</table>

Last update: 20 October 2021