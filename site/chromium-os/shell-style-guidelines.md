---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: shell-style-guidelines
title: Shell Style Guidelines
---

<table>
<tr>

<td>## Moved to <a href="https://chromium.googlesource.com/chromiumos/docs/+/master/styleguide/shell.md">https://chromium.googlesource.com/chromiumos/docs/+/master/styleguide/shell.md</a>. Please update the link that brought you here with the new location.</td>

</tr>
</table>