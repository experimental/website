---
breadcrumbs:
- - /chromium-os
  - Chromium OS
- - /chromium-os/testing
  - Testing Home
- - /chromium-os/testing/moblab
  - MobLab
page_name: faft-testing-with-moblab
title: Running FAFT with MobLab
---

## \*\*\*DEPRECATED, PLEASE VISIT <https://sites.google.com/a/chromium.org/dev/chromium-os/testing/moblab> \*\*\* (20170714)