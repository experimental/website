---
breadcrumbs:
- - /chromium-os
  - Chromium OS
page_name: chrome-os-systems-supporting-android-apps
title: Chrome OS Systems Supporting Android Apps
---

Google Play Store and Android apps are available on many Chrome OS devices since
launching in 2016. To learn more about what we launched in 2016, check out [this
blog
post](https://blog.google/products/chromebooks/the-google-play-store-coming-to/).

The Chromebooks, Chromeboxes, and Chromebases that were launched before 2019
that are able to install Android apps are listed below. Unless specified
elsewhere, all devices that have launched in or after 2019 will support Android
Apps. Roll-out of Android Apps is done on a device-per-device basis as it is
dependent on a number of factors including the hardware platform that the device
is based on and each device must be [compatible with
Android](https://source.android.com/compatibility/). While we won't be able to
bring Android apps to every Chromebook ever made, we're continuing to evaluate
more devices and we'll update this list as new devices are added. Even if your
Chromebook isn't on the list below, it will continue to get other new features
and improvements.

To learn how to install Android apps on supported Chromebooks, [click
here](https://support.google.com/chromebook/answer/7021273).

<table>
<tr>

<td>Manufacturer</td>

<td>Device</td>

<td>Status1</td>

</tr>
<tr>

<td>Acer</td>

<td>Chromebook R11 (CB5-132T, C738T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 11 (R751T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook R13 (CB5-312T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 N7 (C731, C731T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (C771, C771T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 14 (CB3-431)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 14 for Work (CP5-471)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 (CB3-532)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 (CB515-1HT/1H)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (C740)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 (CB5-571 / C910)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (C732, C732T, C732L, C732LT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (CB311-8H, CB311-8HT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 13 (CB713-1W)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 (CB315-1H, CB315-1HT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 311 (C721)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 311 (C733, C733U, C733T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 315 (CB315-2H)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 512 (C851, C851T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 514 (CB514)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 11 (CP311-H1, CP311-1HN)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 13 (CP713-1WN)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 311 (R721T)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 511 (R752T, R752TN)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 512 (R851TN)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Tab 10</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox CXI3</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 715 (CB715-1W, CB715-1WT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 714 (CB714-1W, CB714-1WT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Spin 15 (CP315-1H/1HT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 (CB315-1H/1HT)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebase (CA24I2, CA24V2)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (C730 / CB3-111 / C730E / CB3-131)</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook 15 (CB3-531)</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebox CXI2</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebase 24</td>

<td>Planned</td>

</tr>
<tr>

<td>AOpen</td>

<td>Chromebox Mini</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebase Mini</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox Commercial 2</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox Commercial</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebase Commercial</td>

<td>Planned</td>

</tr>
<tr>

<td>Asus</td>

<td>Chromebook Flip C100PA</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Flip C101PA</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Flip C213</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C202SA</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C300SA / C301SA</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Flip C302</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C204</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C223</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C403</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C423</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C523</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Flip C214</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Flip C434</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox 3 (CN65)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Tablet CT100</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebit CS10</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook C200MA</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook C201PA</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook C300MA</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebox CN62</td>

<td>Planned</td>

</tr>
<tr>

<td>Bobicus</td>

<td>Chromebook 11</td>

<td>Planned</td>

</tr>
<tr>

<td>CTL</td>

<td>NL61 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>J2 / J4 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>J5 Convertible Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook J41 / J41T</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook NL7</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook NL7 / NL7T-360 / NL7TW-360</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook NL7 LTE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox CBx1</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Tablet Tx1 for Education</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>N6 Education Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>Dell</td>

<td>Chromebook 11 (3180)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (5190)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 2-in-1 (3189)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 2-in-1 (5190)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 13 (3380)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 13 (7310)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 3100</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 3100 2-in-1</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 3400</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Inspiron Chromebook 14 2-in-1 (7486)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 (3120)</td>

<td>Planned</td>

</tr>
<tr>

<td>eduGear</td>

<td>Chromebook K Series</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook M Series</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>CMT Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook R Series</td>

<td>Planned</td>

</tr>
<tr>

<td>Edxis</td>

<td>Education Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>Google</td>

<td>Chromebook Pixel (2015)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Pixelbook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Pixel Slate</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Haier</td>

<td>Chromebook 11e</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 C</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook 11 G2</td>

<td>Planned</td>

</tr>
<tr>

<td>Hexa</td>

<td>Chromebook Pi</td>

<td>Planned</td>

</tr>
<tr>

<td>HiSense</td>

<td>Chromebook 11</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>HP</td>

<td>Chromebook 11 G5 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 G5</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 G6 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook x360 11 G1 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 13 G1</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 14 G5</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 G7 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11A G6 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 14A G5</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 15 G1</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook x2</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook x360 11 G2 EE</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook x360 14</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebox G2</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11 G3</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook 11 G4 / G4 EE</td>

<td>Planned</td>

</tr>
<tr>

<td>Chromebook 14 G4</td>

<td>Planned</td>

</tr>
<tr>

<td>Lava</td>

<td>Xolo Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>Lenovo</td>

<td>Thinkpad 11e Chromebook (Gen 3)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>ThinkPad 11e Yoga Chromebook (Gen 3)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>ThinkPad 11e Chromebook (Gen 4)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Thinkpad 11e Yoga Chromebook (Gen 4)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>N22 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>N23 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>100e Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Flex 11 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>N23 Yoga Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>300e Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>500e Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>N42 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Thinkpad 13 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>100e Chromebook 2nd Gen</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>14e Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>300e Chromebook 2nd Gen</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>500e Chromebook 2nd Gen</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>100e Chromebook 2nd Gen MTK</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>300e Chromebook 2nd Gen MTK</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C330</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook S330</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Yoga C630 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C340-11</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook S340-14</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>100S Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>N20 / N20P Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>N21 Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>ThinkPad 11e Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>ThinkPad 11e Yoga Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>ThinkCentre Chromebox</td>

<td>Planned</td>

</tr>
<tr>

<td>Medion</td>

<td>Chromebook S2015</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Akoya S2013</td>

<td>Planned</td>

</tr>
<tr>

<td>M&A</td>

<td>Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>Mercer</td>

<td>Chromebook NL6D</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Multilaser</td>

<td>Chromebook M11C</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>NComputing</td>

<td>Chromebook CX100</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Nexian</td>

<td>Chromebook 11.6”</td>

<td>Stable Channel</td>

</tr>
<tr>
<td> Newline</td>
<td> Chromebox A10</td>
<td> Stable Channel</td>
</tr>
<tr>

<td>PCMerge</td>

<td>Chromebook PCM-116E</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook PCM-116T-432B</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook AL116</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Poin2</td>

<td>Chromebook 11</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 14</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11C</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Positivo</td>

<td>Chromebook CH1190</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook C216B</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Promethean</td>

<td>Chromebox</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Prowise</td>

<td>Chromebook Eduline</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Entryline</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Proline</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Samsung</td>

<td>Chromebook Plus</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Pro</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 3</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Plus (LTE)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook Plus (V2)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 2 11" - XE500C12</td>

<td>Planned</td>

</tr>
<tr>

<td>Sector 5</td>

<td>E1 Rugged Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>E3 Chromebook</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Senkatel</td>

<td>C1101 Chromebook</td>

<td>Planned</td>

</tr>
<tr>

<td>Toshiba</td>

<td>Chromebook 2 (2015)</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 2</td>

<td>Planned</td>

</tr>
<tr>

<td>True IDC</td>

<td>Chromebook 11</td>

<td>Planned</td>

</tr>
<tr>

<td>ViewSonic</td>

<td>NMP660 Chromebox</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Viglen</td>

<td>Chromebook 11</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 360</td>

<td>Stable Channel</td>

</tr>
<tr>

<td>Chromebook 11C</td>

<td>Stable Channel</td>

</tr>
</table>

1. If a device status is marked as available on the Beta channel this does not
mean that it will be available in the Stable channel on the next release of
Chrome OS. For more information on how to move your device between Chromebook
release channels [click
here](https://support.google.com/chromebook/answer/1086915).