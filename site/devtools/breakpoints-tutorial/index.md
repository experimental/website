---
breadcrumbs:
- - /devtools
  - Google Chrome Developer Tools
page_name: breakpoints-tutorial
title: breakpoints-tutorial
---

This page has moved to <http://code.google.com/chrome/devtools>.

You will be automatically redirected there in 10 seconds.

<img alt="image" src="http://www.google.com/chart" height=100 width=300>