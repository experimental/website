---
breadcrumbs:
- - /for-testers
  - For Testers
page_name: command-line-flags
title: How to specify command line flags
---

This page has been merged into the more complete:

<https://sites.google.com/a/chromium.org/dev/developers/how-tos/run-chromium-with-flags>