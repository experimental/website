---
breadcrumbs: []
page_name: for-testers
title: For Testers
---

#### Reporting Bugs

*   [Bug reporting guidelines and how to report
            bugs](/for-testers/bug-reporting-guidelines)
*   [Glossary](/glossary)
*   [Enable logging](/for-testers/enable-logging)
*   [Recording
            traces](/developers/how-tos/trace-event-profiling-tool/recording-tracing-runs)

#### Contributing

*   [Find duplicate bugs](http://code.google.com/p/chromium/issues/list)
*   [Confirm unconfirmed bugs
            ](http://code.google.com/p/chromium/issues/list)
*   [Confirm unconfirmed Mac
            bugs](http://code.google.com/p/chromium/issues/list)
*   [Confirm unconfirmed Linux
            bugs](http://code.google.com/p/chromium/issues/list)
*   [Help us by creating reduced test
            cases](/system/errors/NodeNotFound)
*   [Verify Mac fixed
            bugs](http://code.google.com/p/chromium/issues/list)
*   [Verify Linux fixed
            bugs](http://code.google.com/p/chromium/issues/list)
*   [Frontend testing](/for-testers/frontend-testing)
*   [Backend testing](/system/errors/NodeNotFound)
*   [Firmware testing](/for-testers/faft)
*   [Installer](/for-testers/installer)