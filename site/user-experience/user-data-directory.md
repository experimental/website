---
breadcrumbs:
- - /user-experience
  - User Experience
page_name: user-data-directory
title: User Data Directory
---

This content moved to
<https://chromium.googlesource.com/chromium/src/+/master/docs/user_data_dir.md>.