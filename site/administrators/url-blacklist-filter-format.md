---
breadcrumbs:
- - /administrators
  - Documentation for Administrators
page_name: url-blacklist-filter-format
title: URL Blocklist filter format (legacy link)
---

Please update your link to point to
<https://sites.google.com/a/chromium.org/dev/administrators/url-blocklist-filter-format>
.