---
breadcrumbs: []
page_name: chromium-projects
title: Home
use_title_as_h1: false
---

The Chromium projects include Chromium and Chromium OS, the open-source projects
behind the [Google Chrome](https://www.google.com/chrome) browser and Google
Chrome OS, respectively. This site houses the documentation and code related to
the Chromium projects and is intended for developers interested in learning
about and contributing to the open-source projects.

<div class="two-column-container">
<div class="column">

**[Chromium](/Home)**
Chromium is an open-source browser project that aims to build a safer, faster,
and more stable way for all users to experience the web. This site contains
design documents, architecture overviews, testing information, and more to help
you learn to build and work with the Chromium source code.

<table>
<tr>
<td> <table></td>
<td> <tr></td>
<td> <td><a href="https://www.google.com/chrome"><img alt="https://www.google.com/chrome" src="/chromium-projects/logo_chrome_color_1x_web_32dp.png"></a></td></td>

<td><td>Looking for Google Chrome?</td></td>

<td><td> <a href="https://www.google.com/chrome">Download Google Chrome</a></td></td>
<td> </tr></td>
<td> </table></td>
</tr>
</table>

</div>
<div class="column">

**[Chromium OS](/chromium-os)**

Chromium OS is an open-source project that aims to provide a fast, simple, and
more secure computing experience for people who spend most of their time on the
web. Learn more about the [project
goals](https://googleblog.blogspot.com/2009/11/releasing-chromium-os-open-source.html),
obtain the latest build, and learn how you can get involved, submit code, and
file bugs.

<table>
<tr>
<td> <table></td>
<td> <tr></td>
<td><td> <table></td> </td>
<td><td> <tr></td></td>

<td><td><td><a href="https://www.google.com/chromeos"><img
alt="https://www.google.com/chromeos"
src="/chromium-projects/logo_chrome_color_1x_web_32dp.png"></a></td></td></td>

<td><td><td>Looking for Google Chrome OS devices?</td> </td></td>
<td><td><td> <a href="https://www.google.com/chromeos">Visit the Google Chrome OS site</a></td></td></td>
<td><td> </tr></td></td>
<td><td> </table></td></td>
<td> </tr></td>
<td> </table></td>
</tr>
</table>

</div>
</div>