---
breadcrumbs:
- - /issue-tracking
  - Issue Tracking
page_name: issue-tracking-lifecycle
title: Issue Tracking Lifecycle
---

This article explores how issue tracking works on the Chromium project, covering
the four key process phases.

[TOC]

## The Four Phases

[<img alt="image"
src="/issue-tracking/issue-tracking-lifecycle/Conceptual%20Model.png">](/issue-tracking/issue-tracking-lifecycle/Conceptual%20Model.png)

### Reporting

....

What happens in this step?

Who is involved?

What's the forward objective?

### Routing

....

What happens in this step?

Who is involved?

What's the forward objective?

### Triage

....

What happens in this step?

Who is involved?

What's the forward objective?

### Action

....

What happens in this step?

Who is involved?

What's the forward objective?

## Status

[<img alt="image"
src="/issue-tracking/issue-tracking-lifecycle/State%20Diagram.jpg">](/issue-tracking/issue-tracking-lifecycle/State%20Diagram.jpg)