module.exports = config => {
  config.addWatchTarget('./site/_stylesheets/');
  config.setTemplateFormats(['md']);

  // Copy binary assets over to the dist/ directory.

  // This list must be kept in sync with the lists in //.eleventy.js and
  // //scripts/upload_lobs.py.
  // TODO(dpranke): Figure out how to share these lists to eliminate the
  // duplication and need to keep them in sync.
  let lob_extensions = [
    '.ai',
    '.bin',
    '.bmp',
    '.bz2',
    '.crx',
    '.dia',
    '.msi',
    '.gif',
    '.ico',
    '.jpg',
    '.jpeg',
    '.mp4',
    '.pdf',
    '.png',
    '.PNG',
    '.swf',
    '.tar.gz',
    '.tiff',
    '.webp',
    '.xcf',
    '.xlsx',
    '.zip',
  ];

  // This should basically pick up everything that isn't a .md file
  // or a .sha1.
  // TODO(dpranke): Figure out how to actually enforce this and get
  // rid of the "basically". There has to be a better approach. :).
  let extensions = lob_extensions.concat([
    '.cpp',
    '.csv',
    '.dot',
    '.ebuild',
    '.el',
    '.graffle',
    '.html',
    'patch',
    '.py',
    '.svg',
    '.txt',
    '.xml'
  ]);

  for (let ext of extensions) {
    config.addPassthroughCopy('site/**/*' + ext);
  }

  return {
    dir: {
      input: 'site',
      output: 'build'
    },
  };
};
